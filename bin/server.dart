import 'dart:io';

import 'package:logging/logging.dart';
import 'package:rpc/rpc.dart';
import 'package:s_rpc/services.dart';

final ApiServer _apiServer = new ApiServer();

main() async {
  Logger.root
    ..level = Level.INFO
    ..onRecord.listen(print);

  _apiServer.addApi(new UserService());
  _apiServer.addApi(new PropertyService());
  _apiServer.addApi(new FeedbackService());

  HttpServer server = await HttpServer.bind(InternetAddress.ANY_IP_V4, 4040);
  server.listen(_apiServer.httpRequestHandler);

  print("Server running at ${server.address.host} : ${server.port}");
}