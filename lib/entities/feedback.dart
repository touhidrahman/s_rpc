part of models;

@serializable
class Feedback {
  String id;
  int rate;
  String text;
  String user_id;
  String for_user_id;
}