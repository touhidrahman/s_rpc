part of models;

@serializable
class Property {
  String id;
  int rate;
  String type;
  String title;
  String address;
  int zip_code;
  int status;
  String contact_number;
  String email;
  int room_size;
  int total_size;
  String description;
  DateTime available_from;
  DateTime available_to;
  int rent;
  int utility_cost;
  int dist_from_uni;
  bool smoking;
  bool pets;
  bool parking;
  bool washing_machine;
  bool heating;
  bool fire_alarm;
  bool bike_parking;
  bool garden;
  bool balcony;
  bool internet;
  bool cable_tv;
  bool elec_bill_included;
}