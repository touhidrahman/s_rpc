part of models;

@serializable
class User {
  String id;
  String first_name;
  String last_name;
  String username;
  String password;
  int status;
  String contact_number;
  String gender;
  String city_id;
  String address;
  String photo;
}