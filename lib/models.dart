library models;

import 'package:dson/dson.dart';

part 'entities/user.dart';
part 'entities/feedback.dart';
part 'entities/property.dart';