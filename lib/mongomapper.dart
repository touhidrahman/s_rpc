import 'dart:async';
import 'dart:convert';
import 'dart:mirrors';

import 'package:rpc/rpc.dart';
import 'package:rpc/common.dart';
import 'package:rpc/src/parser.dart';
import 'package:rpc/src/config.dart';
import 'package:mongo_dart/mongo_dart.dart';


// execute before each action
typedef beforeHandler(String id, var filter, var data);

// execute after each action
typedef afterHandler(var data);

class MongoMapper<T> {
  ApiConfigSchema apiConfigSchema;
  String name;
  DbCollection collec;

  List<beforeHandler> beforeGet = [];
  List<beforeHandler> beforePut = [];
  List<beforeHandler> beforePost = [];
  List<beforeHandler> beforeDelete = [];
  List<beforeHandler> beforePatch = [];

  List<afterHandler> afterGet = [];
  List<afterHandler> afterPut = [];
  List<afterHandler> afterPost = [];
  List<afterHandler> afterDelete = [];
  List<afterHandler> afterPatch = [];

  /// new MongoMapper will create a new instance and will use [T] as Type to convert JSON data from mongodb
  MongoMapper({this.collec}) {
    ApiParser parser = new ApiParser();
    apiConfigSchema = parser.parseSchema(reflectClass(T), false);
    name = apiConfigSchema.schemaName;
  }

  toJson(T obj) {
    var json = apiConfigSchema.toResponse(obj);
    json["_id"] = json["id"];
    if (json["_id"] == null)
      json.remove("_id");   // remove _id field from mongo schema
    return json;
  }

  fromJson(var json) {
    try {
      json["id"] = GetStringId(json["_id"]);
      json.remove("_id");
      return apiConfigSchema.fromRequest(json);
    } catch (e, stackTrace) {
      print(e);
      print(stackTrace);
      throw new InternalServerError('Cannont convert body');
    }
  }

  before(beforeHandler handler) {
    beforeDelete.add(handler);
    beforeGet.add(handler);
    beforePatch.add(handler);
    beforePost.add(handler);
    beforePut.add(handler);
  }

  after(afterHandler handler) {
    afterDelete.add(handler);
    afterGet.add(handler);
    afterPatch.add(handler);
    afterPost.add(handler);
    afterPut.add(handler);
  }

  SelectorBuilder _createFilter(String filter, {var id, int limit}) {
    var w = where;  // where querybuilder from rpc package
    var obj = JSON.decode(filter == null ? "{}" : filter);
    if (id != null) {
      w.id(GetObjectId(id));
    }
    if (obj["id"] != null) {
      w.id(GetObjectId(obj["id"]));
      obj.remove("id");
    }
    if (obj["_id"] != null) {
      w.id(GetObjectId(obj["_id"]));
      obj.remove("_id");
    }

    // allow filters from URL query string
    if (obj is Map) {
      obj.forEach((var key, var value) {
        if (value is Map) {
          value.forEach((var k, var v) {
            if (k == 'lt') w.lt(key, v);
            else if (k == "gt") w.gt(key, v);
            else if (k == "gte") w.gte(key, v);
            else if (k == "lte") w.lte(key, v);
            else if (k == "eq") w.eq(key, v);
            else if (k == "ne") w.ne(key, v);
            else if (k == "match") w.match(key, v);
          });
        } else {
          w.eq(key, value);
        }
      });
    }
    if (limit != null) w.limit(limit);
    return w;
  }

  applyHandler(List handlers, List arg) {
    var ret;
    for (var f in handlers) {
      ret = Function.apply(f, arg);
      if (ret != null) {
        return ret;
      }
    }
    return null;
  }

  /// GET http://localhost/<module>/<ver>/api?filter=<filter>&limit=<num>
  @ApiMethod(path: "api", method: 'GET')
  Future<List<T>> getModel({String filter, int limit}) async {
    var ret = applyHandler(beforeGet, [null, filter, null]);

    if (ret != null) return ret;

    Stream<Map> cursor = collec.find(_createFilter(filter, limit: limit));
    List<T> l = [];
    await cursor.forEach((element) {
      var e = fromJson(element);
      l.add(e);
    });

    ret = applyHandler(afterGet, [l]);
    if (ret != null) return ret;

    return l;
  }

  /// GET http://localhost/<module>/<ver>/api/<id>?filter=<filter>&limit=<num>
  @ApiMethod(path: "api/{id}", method: 'GET')
  Future<T> getModelById(String id, {String filter, int limit}) async {
    var ret = applyHandler(beforeGet, [id, filter, null]);
    if (ret != null) return ret;

    var json;
    json = await collec.findOne(_createFilter(filter, limit: limit, id: id));

    if (json == null) {
      throw new NotFoundError("no object with id : ${id}");
    }
    T obj = fromJson(json);

    ret = applyHandler(afterGet, [obj]);
    if (ret != null) return ret;

    return obj;
  }

  /// POST http://localhost/<module>/<ver>/api
  @ApiMethod(path: "api", method: 'POST')
  Future<T> postModel(T model) async {
    var ret = applyHandler(beforePost, [null, null, model]);
    if (ret != null) return ret;

    var json = toJson(model);
    var tmp = await collec.insert(json);
    tmp = await collec.findOne(json);
    T m = fromJson(tmp);

    ret = applyHandler(beforeGet, [m]);
    if (ret != null) return ret;

    return m;
  }

  /// DELETE http://localhost/<module>/<ver>/api?filter=<filter>
  @ApiMethod(path: "api/{id}", method: 'DELETE')
  Future<Map<String, String>> deleteModelId(String id) async {
    var ret = applyHandler(beforeDelete, [id, null, null]);
    if (ret != null) return ret;

    await collec.remove(_createFilter("{}", id: id));

    ret = applyHandler(afterDelete, [null]);
    if (ret != null) return ret;

    return {"status": "OK"};
  }

  /// PUT http://localhost/<module>/<ver>/api
  @ApiMethod(path: "api/{id}", method: 'PUT')
  Future<T> updateModel(String id, T model) async {
    var ret = applyHandler(beforePut, [null, null, model]);
    if (ret != null) return ret;

    try {
      var tmp = await collec.findOne(_createFilter("{}", id: id));
      tmp.addAll(toJson(model));
      if (tmp["_id"] != null && tmp["_id"] is String)
        tmp["_id"] = StringToId(tmp["_id"]);
      tmp = await collec.save(tmp);
    } catch (e, stackTrace) {
      print("${e}, ${stackTrace}");
      throw new InternalServerError('Error updating data');
    }


    ret = applyHandler(afterPut, [model]);
    if (ret != null) return ret;

    return model;
  }

  /// PATCH http://localhost/<module>/<ver>/api
  @ApiMethod(path: "api/{id}", method: 'PATCH')
  VoidMessage patchModel(String id, T model) {
    collec.update(_createFilter("{}", id: id), toJson(model));
    return null;
  }
}

// convert a HexString format id into a ObjectId
ObjectId StringToId (String id) => new ObjectId.fromHexString(id);

// return a ObjectId from id
// If id is an ObjectId, it will return id
// If id is a String, it will return the ObjectId conversion of the String
ObjectId GetObjectId (var id) => id is ObjectId ? id : StringToId(id);

// return a String representation of id
String GetStringId (var id) => id is ObjectId ? IdToHexString(id) : id;

// convert a ObjectId to a String
String IdToHexString (ObjectId id) => id.toHexString();