library services;

import 'dart:async';
import 'package:rpc/rpc.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:dson/dson.dart';
import 'models.dart';
import 'mongomapper.dart';

part 'services/userservices.dart';
part 'services/feedbackservices.dart';
part 'services/propertyservices.dart';

//const DBURL = "mongodb://localhost:27017/myproject";
const DBURL = "mongodb://root:my5tr0ngp455@ds137540.mlab.com:37540/studierent";