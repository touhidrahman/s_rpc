part of services;

@ApiClass(
    version: "v1",
    name: "feedbacks",
    title: 'Feedback Management Service'
)
class FeedbackService {
  var db;

  @ApiResource()
  MongoMapper<Feedback> feedbacks = new MongoMapper<Feedback>();

  FeedbackService() {
    db = new Db(DBURL);
    db.open().then((_) {
      feedbacks.collec = db.collection("feedbacks");
    });
  }

}
