part of services;

@ApiClass(
    version: "v1",
    name: "properties",
    title: 'Property Management Service'
)
class PropertyService {
  var db;

  @ApiResource()
  MongoMapper<Property> properties = new MongoMapper<Property>();

  PropertyService() {
    db = new Db(DBURL);
    db.open().then((_) {
      properties.collec = db.collection("properties");
    });
  }
}
