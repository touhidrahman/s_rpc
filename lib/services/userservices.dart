part of services;

@ApiClass(
    version: "v1",
    name: "users",
    title: 'User Management Service'
)
class UserService {
  var db;

  @ApiResource()
  MongoMapper<User> users = new MongoMapper<User>();

  UserService() {
    db = new Db(DBURL);
    db.open().then((_) {
      users.collec = db.collection("users");
    });
  }
}